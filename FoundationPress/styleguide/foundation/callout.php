
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Callout</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<div class="callout">
				<h5>This is a callout.</h5>
				<p>
					It has an easy to override visual style, and is appropriately subdued.
				</p>
				<a href="#">It's dangerous to go alone, take this.</a>
			</div>

			<div class="callout secondary">
				<h5>This is a secondary callout</h5>
				<p>
					It has an easy to override visual style, and is appropriately subdued.
				</p>
				<a href="#">It's dangerous to go alone, take this.</a>
			</div>

			<div class="callout success">
				<h5>This is a success callout</h5>
				<p>
					It has an easy to override visual style, and is appropriately subdued.
				</p>
				<a href="#">It's dangerous to go alone, take this.</a>
			</div>

			<div class="callout warning">
				<h5>This is a warning callout</h5>
				<p>
					It has an easy to override visual style, and is appropriately subdued.
				</p>
				<a href="#">It's dangerous to go alone, take this.</a>
			</div>

			<div class="callout alert">
				<h5>This is an alert callout</h5>
				<p>
					It has an easy to override visual style, and is appropriately subdued.
				</p>
				<a href="#">It's dangerous to go alone, take this.</a>
			</div>
		</div>
	</div>

	
