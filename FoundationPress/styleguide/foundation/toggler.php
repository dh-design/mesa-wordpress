
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Toggler</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<p>
				<a data-toggle="menuBar">Expand!</a>
			</p>

			<ul class="menu" id="menuBar" data-toggler=".expanded">
				<li>
					<a href="#">One</a>
				</li>
				<li>
					<a href="#">Two</a>
				</li>
				<li>
					<a href="#">Three</a>
				</li>
				<li>
					<a href="#">Four</a>
				</li>
			</ul>
		</div>
	</div>
