
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Magellan</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<ul class="horizontal menu" data-magellan>
				<li>
					<a href="#first">First Arrival</a>
				</li>
				<li>
					<a href="#second">Second Arrival</a>
				</li>
				<li>
					<a href="#third">Third Arrival</a>
				</li>
			</ul>
			<div class="sections">
				<section id="first" data-magellan-target="first">
					<h4>First section</h4>
					<p>
						Duis scelerisque ligula ut metus rhoncus scelerisque. Integer ut egestas metus. Nulla facilisi. Aenean luctus magna lobortis ligula rhoncus, sit amet lacinia lorem sagittis. Sed ultrices at metus id aliquet. Vestibulum in condimentum quam, id ornare erat. Vivamus nec justo quis ex fringilla condimentum ac non quam.
					</p>
				</section>
				<section id="second" data-magellan-target="second">
					<h4>Second section</h4>
					<p>
						Sed vulputate, felis interdum molestie viverra, neque urna placerat dui, ac efficitur est magna eu tellus. Nunc sodales consequat eros at bibendum. Vestibulum hendrerit gravida elit non eleifend. Nunc at vehicula ipsum. Vestibulum eu suscipit felis. Proin ipsum felis, consequat congue quam ac, efficitur tincidunt ex. Morbi accumsan sem iaculis nunc malesuada tincidunt.
					</p>
				</section>
				<section id="third" data-magellan-target="third">
					<h4>Second section</h4>
					<p>
						Aliquam orci orci, maximus a pulvinar id, tincidunt a neque. Suspendisse eros diam, finibus et faucibus ac, suscipit feugiat orci. Morbi scelerisque sem id blandit malesuada. Donec suscipit tincidunt dolor in blandit. Nam rhoncus risus vitae lacinia dictum. Cras lobortis, nulla non faucibus mattis, tellus nibh condimentum eros, posuere volutpat arcu risus vel ante. In ut ullamcorper eros, et vestibulum risus. Fusce auctor risus vitae diam viverra tincidunt.
					</p>
				</section>
			</div>
		</div>
	</div>