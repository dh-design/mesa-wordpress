
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Tabs</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<ul class="tabs" data-tabs id="example-tabs">
				<li class="tabs-title is-active">
					<a href="#panel1" aria-selected="true">Tab 1</a>
				</li>
				<li class="tabs-title">
					<a href="#panel2">Tab 2</a>
				</li>
				<li class="tabs-title">
					<a href="#panel3">Tab 3</a>
				</li>
				<li class="tabs-title">
					<a href="#panel4">Tab 4</a>
				</li>
				<li class="tabs-title">
					<a href="#panel5">Tab 5</a>
				</li>
				<li class="tabs-title">
					<a href="#panel6">Tab 6</a>
				</li>
			</ul>

			<div class="tabs-content" data-tabs-content="example-tabs">
				<div class="tabs-panel is-active" id="panel1">
					<p>one</p>
					<p>Check me out! I'm a super cool Tab panel with text content!</p>
				</div>
				<div class="tabs-panel" id="panel2">
					<p>two</p>
					<img class="thumbnail" alt="" data-interchange="
						[http://placehold.it/200, 
						[http://placehold.it/300, medium], 
						[http://placehold.it/500, large]">
				</div>
				<div class="tabs-panel" id="panel3">
					<p>three</p>
					<p>Check me out! I'm a super cool Tab panel with text content!</p>
				</div>
				<div class="tabs-panel" id="panel4">
					<p>four</p>
					<img class="thumbnail" alt="" data-interchange="
						[http://placehold.it/200, 
						[http://placehold.it/300, medium], 
						[http://placehold.it/500, large]">
				</div>
				<div class="tabs-panel" id="panel5">
					<p>five</p>
					<p>Check me out! I'm a super cool Tab panel with text content!</p>
				</div>
				<div class="tabs-panel" id="panel6">
					<p>six</p>
					<img class="thumbnail" alt="" data-interchange="
						[http://placehold.it/200, 
						[http://placehold.it/300, medium], 
						[http://placehold.it/500, large]">
				</div>
			</div>
		</div>
	</div>

