
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Thumbnail</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<div class="row">
				<div class="small-4 columns">
					<img class="thumbnail" alt="" data-interchange="
						[http://placehold.it/200, 
						[http://placehold.it/300, medium], 
						[http://placehold.it/500, large]">
				<div class="small-4 columns">
					<img class="thumbnail" alt="" data-interchange="
						[http://placehold.it/200, 
						[http://placehold.it/300, medium], 
						[http://placehold.it/500, large]">
				<div class="small-4 columns">
					<img class="thumbnail" alt="" data-interchange="
						[http://placehold.it/200, 
						[http://placehold.it/300, medium], 
						[http://placehold.it/500, large]">
			</div>
		</div>
	</div>
