
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Top Bar</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<div class="top-bar">
				<div class="top-bar-left">
					<ul class="dropdown menu" data-dropdown-menu>
						<li class="menu-text">Site Title</li>
						<li class="has-submenu">
							<a href="#">One</a>
							<ul class="submenu menu vertical" data-submenu>
								<li>
									<a href="#">One</a>
								</li>
								<li>
									<a href="#">Two</a>
								</li>
								<li>
									<a href="#">Three</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#">Two</a>
						</li>
						<li>
							<a href="#">Three</a>
						</li>
					</ul>
				</div>
				<div class="top-bar-right">
					<ul class="menu">
						<li>
							<input type="search" placeholder="Search"></li>
						<li>
							<button type="button" class="button">Search</button>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

