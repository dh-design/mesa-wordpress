
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Dropdown Menu</h4>
            </div>
        </div>
    </div>
    <div class="row snippet">
	    <div class="small-12 columns">
	    	<ul class="dropdown menu" data-dropdown-menu>
	    		<li>
	    			<a>Item 1</a>
	    			<ul class="menu">
	    				<li>
	    					<a href="#">Item 1A Loooong</a>
	    				</li>
	    				<li>
	    					<a href='#'>Item 1 sub</a>
	    					<ul class='menu'>
	    						<li>
	    							<a href='#'>Item 1 subA</a>
	    						</li>
	    						<li>
	    							<a href='#'>Item 1 subB</a>
	    						</li>
	    						<li>
	    							<a href='#'>Item 1 sub</a>
	    							<ul class='menu'>
	    								<li>
	    									<a href='#'>Item 1 subA</a>
	    								</li>
	    								<li>
	    									<a href='#'>Item 1 subB</a>
	    								</li>
	    							</ul>
	    						</li>
	    						<li>
	    							<a href='#'>Item 1 sub</a>
	    							<ul class='menu'>
	    								<li>
	    									<a href='#'>Item 1 subA</a>
	    								</li>
	    							</ul>
	    						</li>
	    					</ul>
	    				</li>
	    				<li>
	    					<a href="#">Item 1B</a>
	    				</li>
	    			</ul>
	    		</li>
	    		<li>
	    			<a href="#">Item 2</a>
	    			<ul class="menu">
	    				<li>
	    					<a href="#">Item 2A</a>
	    				</li>
	    				<li>
	    					<a href="#">Item 2B</a>
	    				</li>
	    			</ul>
	    		</li>
	    		<li>
	    			<a href="#">Item 3</a>
	    		</li>
	    		<li>
	    			<a href='#'>Item 4</a>
	    		</li>
	    	</ul>
	    </div>
    </div>
