
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Button</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<!-- Anchors (links) -->
			<a href="#" class="button">Learn More</a>
			<a href="#features" class="button">View All Features</a>

			<!-- Buttons (actions) -->
			<button type="button" class="success button">Save</button>
			<button type="button" class="alert button">Delete</button>

			<a class="tiny button" href="#">So Tiny</a>
			<a class="small button" href="#">So Small</a>
			<a class="large button" href="#">So Large</a>
			<a class="expanded button" href="#">Such Expand</a>

			<div class="button-group">
			  	<a class="button">One</a>
			  	<a class="button">Two</a>
			  	<a class="button">Three</a>
			</div>
		</div>
	</div>

	