
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Interchange</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<img data-interchange="
				[http://placehold.it/600x300, 
				[http://placehold.it/1000x500, medium], 
				[http://placehold.it/1400x700, large]">
		</div>
	</div>