
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Drilldown Menu</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<ul class="vertical menu" data-drilldown style="width: 200px" id="m1">
				<li>
					<a href="#">Item 1</a>
					<ul class="vertical menu" id="m2">
						<li>
							<a href="#">Item 1A</a>
							<ul class="vertical menu" id="m3">
								<li>
									<a href="#">Item 1Aa</a>
								</li>
								<li>
									<a href="#">Item 1Ba</a>
								</li>
								<li>
									<a href="#">Item 1Ca</a>
								</li>
								<li>
									<a href="#">Item 1Da</a>
								</li>
								<li>
									<a href="#">Item 1Ea</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#">Item 1B</a>
						</li>
						<li>
							<a href="#">Item 1C</a>
						</li>
						<li>
							<a href="#">Item 1D</a>
						</li>
						<li>
							<a href="#">Item 1E</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="#">Item 2</a>
					<ul class="vertical menu">
						<li>
							<a href="#">Item 2A</a>
						</li>
						<li>
							<a href="#">Item 2B</a>
						</li>
						<li>
							<a href="#">Item 2C</a>
						</li>
						<li>
							<a href="#">Item 2D</a>
						</li>
						<li>
							<a href="#">Item 2E</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="#">Item 3</a>
					<ul class="vertical menu">
						<li>
							<a href="#">Item 3A</a>
						</li>
						<li>
							<a href="#">Item 3B</a>
						</li>
						<li>
							<a href="#">Item 3C</a>
						</li>
						<li>
							<a href="#">Item 3D</a>
						</li>
						<li>
							<a href="#">Item 3E</a>
						</li>
					</ul>
				</li>
				<li>
					<a href='#'>Item 4</a>
				</li>
			</ul>
		</div>
	</div>
