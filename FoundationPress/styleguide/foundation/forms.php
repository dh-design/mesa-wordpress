
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
            <h4>Forms <small>(Custom Forms -- Extras)</small></h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<form class="custom-form">
				<div class="row">
					<div class="columns large-12">
						<label>
							Input Label
							<input type="text" placeholder=".small-12.columns" aria-describedby="exampleHelpText"></label>
							<p class="help-text" id="exampleHelpText">Here's how you use this input field!</p>
						<label>
							How many puppies?
							<input type="number" value="100"></label>
						<label>
							What books did you read over summer break?
							<textarea placeholder="None"></textarea>
						</label>
						<label>
							Select Menu
							<select>
								<option value="husker">Husker</option>
								<option value="starbuck">Starbuck</option>
								<option value="hotdog">Hot Dog</option>
								<option value="apollo">Apollo</option>
							</select>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<div class="spacer-small"></div>
					</div>
				</div>
				<div class="row">
					<div class="large-6 columns">
			            <input type="radio" name="radioExample" value="1" id="radio1" checked="checked"><label for="radio1">Radio 1</label><br>
			            <input type="radio" name="radioExample" value="2" id="radio2"><label for="radio2">Radio 2</label><br>
			            <input type="radio" name="radioExample" value="3" id="radio3" disabled="disabled"><label for="radio3">Radio 3 (disabled)</label>
					</div>
					<div class="large-6 columns">
						 <input id="checkbox5" type="checkbox" checked="checked"><label for="checkbox5">Checkbox 1</label><br>
            			<input id="checkbox6" type="checkbox"><label for="checkbox6">Checkbox 2</label><br>
            			<input id="checkbox7" type="checkbox" disabled="disabled"><label for="checkbox7">Checkbox 3 (disabled)</label>
					</div>
				</div>
				<div class="row">
					<div class="small-3 columns">
						<label for="middle-label" class="text-right middle">Label</label>
					</div>
					<div class="small-9 columns">
						<input type="text" id="middle-label" placeholder="Right- and middle-aligned text input">
					</div>
				</div>
				<div class="input-group">
					<span class="input-group-label">$</span>
					<input class="input-group-field" type="url">	
					<a class="input-group-button button">Submit</a>
				</div>
			</form>
		</div>
	</div>
