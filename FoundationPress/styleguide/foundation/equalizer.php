
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Equalizer</h4>
            </div>
        </div>
    </div>
	<div class="row snippet">
		<div class="small-12 columns">
			<div class="row" data-equalizer data-equalize-on="medium" id="test-eq">
				<div class="medium-4 columns">
					<div class="callout" data-equalizer-watch>
						<img class="thumbnail" alt="" data-interchange="
							[http://placehold.it/200, 
							[http://placehold.it/400, medium], 
							[http://placehold.it/600, large]">
					</div>
				</div>
				<div class="medium-4 columns">
					<div class="callout" data-equalizer-watch>
						<p>Pellentesque habitant morbi tristique senectus et netus et, ante.</p>
					</div>
				</div>
				<div class="medium-4 columns">
					<div class="callout" data-equalizer-watch>
						<img class="thumbnail" alt="" data-interchange="
							[http://placehold.it/200x100, 
							[http://placehold.it/400x200, medium], 
							[http://placehold.it/600x300, large]">
					</div>
				</div>
			</div>
		</div>
	</div>

	
