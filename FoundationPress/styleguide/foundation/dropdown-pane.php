
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Dropdown Pane</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<button class="button" type="button" data-toggle="example-dropdown">Toggle Dropdown</button>
			<div class="dropdown-pane" id="example-dropdown" data-dropdown>Just some junk that needs to be said. Or not. Your choice.</div>
		</div>
	</div>
