
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Close Button</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<div class="callout">
				<button class="close-button" aria-label="Close alert" type="button">
					<span aria-hidden="true">&times;</span>
				</button>
				<p>This is a static close button example.</p>
			</div>
		</div>
	</div>

