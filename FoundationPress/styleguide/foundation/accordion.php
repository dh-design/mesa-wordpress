
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Accordion</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<ul class="accordion" data-accordion role="tablist">
				<li class="accordion-item is-active">
					<!-- The tab title needs role="tab", an href, a unique ID, and aria-controls. -->			
					<a href="#panel1d" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">Accordion 1</a>
					<!-- The content pane needs an ID that matches the above href, role="tabpanel", data-tab-content, and aria-labelledby. -->			
					<div id="panel1d" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">Panel 1. Lorem ipsum dolor</div>
				</li>
				<li class="accordion-item">
					<!-- The tab title needs role="tab", an href, a unique ID, and aria-controls. -->			
					<a href="#panel1d" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">Accordion 1</a>
					<!-- The content pane needs an ID that matches the above href, role="tabpanel", data-tab-content, and aria-labelledby. -->			
					<div id="panel1d" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">Panel 2. Lorem ipsum dolor</div>
				</li>
				<li class="accordion-item">
					<!-- The tab title needs role="tab", an href, a unique ID, and aria-controls. -->			
					<a href="#panel1d" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">Accordion 1</a>
					<!-- The content pane needs an ID that matches the above href, role="tabpanel", data-tab-content, and aria-labelledby. -->			
					<div id="panel1d" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">Panel 3. Lorem ipsum dolor</div>
				</li>
			</ul>
		</div>
	</div>
