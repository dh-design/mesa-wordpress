
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Responsive Menu</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<ul class="vertical medium-horizontal menu">
				<li>
					<a href="#">Item 1</a>
				</li>
				<li>
					<a href="#">Item 2</a>
				</li>
				<li>
					<a href="#">Item 3</a>
				</li>
			</ul>
		</div>
	</div>
