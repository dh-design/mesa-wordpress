
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Grid</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<h4><small><strong>Responsive Grid</strong></small></h4>
			<div class="row display">
				<div class="small-2 medium-3 large-4 columns">
					<span class="show-for-small-only">2</span>
					<span class="show-for-medium-only">3</span>
					<span class="show-for-large-up">4</span>
				</div>
				<div class="small-4 medium-5 large-3 columns ">
					<span class="show-for-small-only">4</span>
					<span class="show-for-medium-only">5</span>
					<span class="show-for-large-up">3</span>
				</div>
				<div class="small-6 medium-4 large-5 columns ">
					<span class="show-for-small-only">6</span>
					<span class="show-for-medium-only">4</span>
					<span class="show-for-large-up">5</span>
				</div>
			</div>

			<h4><small><strong>Offsets</strong></small></h4>
			<div class="row display">
				<div class="medium-1 columns">1</div>
				<div class="medium-8 medium-offset-3 columns">8, offset 3</div>
			</div>

			<h4><small><strong>Centered</strong></small></h4>
			<div class="row display">
				<div class="small-6 medium-centered columns">6 centered</div>
			</div>

			<h4><small><strong>Source Ordering</strong></small></h4>
			<div class="row display">
				<div class="large-9 large-push-3 columns">9</div>
				<div class="large-3 large-pull-9 columns">3, last</div>
			</div>
		</div>
	</div>

