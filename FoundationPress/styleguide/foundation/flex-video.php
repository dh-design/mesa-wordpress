
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Flex Video</h4>
            </div>
        </div>
    </div> 		
	<div class="row">
		<div class="columns medium-6"> 
            <div class="flex-video widescreen youtube">
                <iframe width="420" height="315" src="https://www.youtube.com/embed/hw-s5BAzp0g" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="columns medium-6"> 
            <div class="flex-video widescreen vimeo">
                <iframe src="https://player.vimeo.com/video/162302586" width="420" height="315" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
            </div>
        </div>
	</div>