
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Reveal</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<p>
				<a data-open="exampleModal1">Click me for a modal</a>
			</p>

			<div class="reveal" id="exampleModal1" data-reveal>
				<h1>Awesome. I Have It.</h1>
				<p class="lead">Your couch. It is mine.</p>
				<p>
					I'm a cool paragraph that lives inside of an even cooler modal. Wins!
				</p>
				<button class="close-button" data-close aria-label="Close reveal" type="button">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</div>
	</div>
