
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Menu</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<ul class="menu">
				<li>
					<a href="#">One</a>
				</li>
				<li>
					<a href="#">Two</a>
				</li>
				<li>
					<a href="#">Three</a>
				</li>
				<li>
					<a href="#">Four</a>
				</li>
			</ul>

			<ul class="menu icon-top">
				<li>
					<a href="#"> <i class="fi-list"></i>
						<span>One</span>
					</a>
				</li>
				<li>
					<a href="#"> <i class="fi-list"></i>
						<span>Two</span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fi-list"></i>
						<span>Three</span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fi-list"></i>
						<span>Four</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
