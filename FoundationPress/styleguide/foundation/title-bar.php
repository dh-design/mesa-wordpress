
	<div class="row">
        <div class="columns large-12">           
            <div class="section_title">
                <h4>Title Bar &amp; Offcanvas</h4>
            </div>
        </div>
    </div> 		
	<div class="row snippet">
		<div class="small-12 columns">
			<div class="off-canvas-wrapper">
				<div class="off-canvas position-left" id="offCanvas" data-off-canvas data-transition="overlap" data-transition-time="500">

			        <!-- Close button -->
			        <button class="close-button" aria-label="Close menu" type="button" data-close>
			          <span aria-hidden="true">&times;</span>
			        </button>

			        <!-- Menu -->
			        <ul class="vertical menu">
			          <li><a href="#">Foundation</a></li>
			          <li><a href="#">Dot</a></li>
			          <li><a href="#">ZURB</a></li>
			          <li><a href="#">Com</a></li>
			          <li><a href="#">Slash</a></li>
			          <li><a href="#">Sites</a></li>
			        </ul>

		      	</div>
		      	<div class="off-canvas position-right" id="offCanvas2" data-off-canvas data-transition="push" data-transition-time="500">

			        <!-- Close button -->
			        <button class="close-button" aria-label="Close menu" type="button" data-close>
			          <span aria-hidden="true">&times;</span>
			        </button>

			        <!-- Menu -->
			        <ul class="vertical menu">
			          <li><a href="#">Foundation</a></li>
			          <li><a href="#">Dot</a></li>
			          <li><a href="#">ZURB</a></li>
			          <li><a href="#">Com</a></li>
			          <li><a href="#">Slash</a></li>
			          <li><a href="#">Sites</a></li>
			        </ul>

		      	</div>
			    <div class="off-canvas-content" data-off-canvas-content>
					<div class="title-bar">
						<div class="title-bar-left">
							<a class="menu-icon" href="javascript:void(0);" data-toggle="offCanvas"></a>
							<span class="title-bar-title">Foundation</span>
						</div>
						<div class="title-bar-right">
							<a class="menu-icon" href="javascript:void(0);" data-toggle="offCanvas2"></a>
						</div>
					</div>
					<div style="min-height:300px;"></div>
				</div>
			</div>
		</div>
	</div>