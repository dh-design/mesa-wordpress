
    <div class="row">
        <div class="columns large-12">
        	<div class="section_title">
                <h4>Icons</h4>
            </div>
        </div>
    </div>
    <div class="row">
		<div class="small-12 columns">
			<h4 class="sg_pattern_heading">Standard Icons</h4>
			<div class="row small-up-2 medium-up-3 large-up-4 sg_icons">
				<div class="column"><i class="icon-stop"></i> <kbd>icon-stop</kbd></div>
				<div class="column"><i class="icon-circle"></i> <kbd>icon-circle</kbd></div>
				<div class="column"><i class="icon-plus"></i> <kbd>icon-plus</kbd></div>
				<div class="column"><i class="icon-minus"></i> <kbd>icon-minus</kbd></div>
				<div class="column"><i class="icon-check"></i> <kbd>icon-check</kbd></div>
				<div class="column"><i class="icon-close"></i> <kbd>icon-close</kbd></div>
				<div class="column"><i class="icon-search"></i> <kbd>icon-search</kbd></div>
				<div class="column"><i class="icon-envelope"></i> <kbd>icon-envelope</kbd></div>
				<div class="column"><i class="icon-phone"></i> <kbd>icon-phone</kbd></div>
				<div class="column"><i class="icon-map-marker"></i> <kbd>icon-map-marker</kbd></div>
				<div class="column"><i class="icon-compass"></i> <kbd>icon-compass</kbd></div>
				<div class="column"><i class="icon-tag"></i> <kbd>icon-tag</kbd></div>
				<div class="column"><i class="icon-print"></i> <kbd>icon-print</kbd></div>
				<div class="column"><i class="icon-cog"></i> <kbd>icon-cog</kbd></div>
				<div class="column"><i class="icon-th-large"></i> <kbd>icon-th-large</kbd></div>
				<div class="column"><i class="icon-list"></i> <kbd>icon-list</kbd></div>
				<div class="column"><i class="icon-sort"></i> <kbd>icon-sort</kbd></div>
				<div class="column"><i class="icon-caret-down"></i> <kbd>icon-caret-down</kbd></div>
				<div class="column"><i class="icon-caret-left"></i> <kbd>icon-caret-left</kbd></div>
				<div class="column"><i class="icon-caret-right"></i> <kbd>icon-caret-right</kbd></div>
				<div class="column"><i class="icon-caret-up"></i> <kbd>icon-caret-up</kbd></div>
				<div class="column"><i class="icon-chevron-down"></i> <kbd>icon-chevron-down</kbd></div>
				<div class="column"><i class="icon-chevron-left"></i> <kbd>icon-chevron-left</kbd></div>
				<div class="column"><i class="icon-chevron-right"></i> <kbd>icon-chevron-right</kbd></div>
				<div class="column"><i class="icon-chevron-up"></i> <kbd>icon-chevron-up</kbd></div>
			</div>

			<h4 class="sg_pattern_heading">Social Icons</h4>
			<div class="row small-up-2 medium-up-3 large-up-4 sg_icons">
				<div class="column"><i class="icon-facebook"></i> <kbd>icon-facebook</kbd></div>
				<div class="column"><i class="icon-facebook-official"></i> <kbd>icon-facebook-official</kbd></div>
				<div class="column"><i class="icon-facebook-square"></i> <kbd>icon-facebook-square</kbd></div>
				<div class="column"><i class="icon-twitter"></i> <kbd>icon-twitter</kbd></div>
				<div class="column"><i class="icon-twitter-square"></i> <kbd>icon-twitter-square</kbd></div>
				<div class="column"><i class="icon-linkedin"></i> <kbd>icon-linkedin</kbd></div>
				<div class="column"><i class="icon-linkedin-square"></i> <kbd>icon-linkedin-square</kbd></div>
				<div class="column"><i class="icon-google-plus"></i> <kbd>icon-google-plus</kbd></div>
				<div class="column"><i class="icon-google-plus-square"></i> <kbd>icon-google-plus-square</kbd></div>
				<div class="column"><i class="icon-youtube"></i> <kbd>icon-youtube</kbd></div>
				<div class="column"><i class="icon-youtube-square"></i> <kbd>icon-youtube-square</kbd></div>
				<div class="column"><i class="icon-youtube-play"></i> <kbd>icon-youtube-play</kbd></div>
				<div class="column"><i class="icon-pinterest-p"></i> <kbd>icon-pinterest-p</kbd></div>
				<div class="column"><i class="icon-pinterest"></i> <kbd>icon-pinterest</kbd></div>
				<div class="column"><i class="icon-pinterest-square"></i> <kbd>icon-pinterest-square</kbd></div>
				<div class="column"><i class="icon-instagram"></i> <kbd>icon-instagram</kbd></div>
			</div>
			<a href="/lib/styles/fonts/icons-reference.html" class="arrow_link" target="_blank">View All</a>
		</div>
	</div>

	<div class="row">
		<div class="small-12 columns">
			<h4 class="sg_pattern_heading">Favicon</h4>
			<img src="<?php bloginfo('template_url'); ?>/lib/images/icons/favicon.ico" alt="" /> &nbsp; <img src="<?php bloginfo('template_url'); ?>/lib/images/icons/apple-touch-icon.png" alt="" />
		</div>
	</div>



		