
    <div class="row">
        <div class="small-12 columns">
            <div class="section_title">
                <h4>Colors</h4>
            </div>      
            <h4 class="sg_pattern_heading">Accent</h4>
            <ul class="sg_colors">
                <li>
                    <span class="accent primary-color-bkg"></span>
                    <p><em></em><br><kbd>$primary-color</kbd></p>
                </li>
                <li>
                    <span class="accent secondary-color-bkg"></span>
                    <p><em></em><br><kbd>$secondary-color</kbd></p>
                </li>
                <li>
                    <span class="accent link-color-bkg"></span>
                    <p><em></em><br><kbd>$link-color</kbd></p>
                </li>
                <li>
                    <span class="accent alert-color-bkg"></span>
                    <p><em></em><br><kbd>$alert-color</kbd></p>
                </li>
                <li>
                    <span class="accent warning-color-bkg"></span>
                    <p><em></em><br><kbd>$warning-color</kbd></p>
                </li>
                <li>
                    <span class="accent info-color-bkg"></span>
                    <p><em></em><br><kbd>$info-color</kbd></p>
                </li>
            </ul>
            <div class="spacer-small"></div>
            <h4 class="sg_pattern_heading">Base Light</h4>
            <ul class="sg_colors">
                <li>
                    <span class="base white-color-bkg"></span>
                    <p><em></em><br><kbd>$white</kbd></p>
                </li>
                <li>
                    <span class="base ghost-color-bkg"></span>
                    <p><em></em><br><kbd>$ghost</kbd></p>
                </li>
                <li>
                    <span class="base snow-color-bkg"></span>
                    <p><em></em><br><kbd>$snow</kbd></p>
                </li>
                <li>
                    <span class="base vapor-color-bkg"></span>
                    <p><em></em><br><kbd>$vapor</kbd></p>
                </li>
                <li>
                    <span class="base whitesmoke-color-bkg"></span>
                    <p><em></em><br><kbd>$whitesmoke</kbd></p>
                </li>
                <li>
                    <span class="base silver-color-bkg"></span>
                    <p><em></em><br><kbd>$silver</kbd></p>
                </li>
                <li>
                    <span class="base smoke-color-bkg"></span>
                    <p><em></em><br><kbd>$smoke</kbd></p>
                </li>
                <li>
                    <span class="base gainsboro-color-bkg"></span>
                    <p><em></em><br><kbd>$gainsboro</kbd></p>
                </li>
                <li>
                    <span class="base base-color-bkg"></span>
                    <p><em></em><br><kbd>$base</kbd></p>
                </li>
                
            </ul>
            <div class="spacer-small"></div>
            <h4 class="sg_pattern_heading">Base Dark</h4>
            <ul class="sg_colors">
                <li>
                    <span class="base aluminum-color-bkg"></span>
                    <p><em></em><br><kbd>$aluminum</kbd></p>
                </li>
                <li>
                    <span class="base jumbo-color-bkg"></span>
                    <p><em></em><br><kbd>$jumbo</kbd></p>
                </li>
                <li>
                    <span class="base monsoon-color-bkg"></span>
                    <p><em></em><br><kbd>$monsoon</kbd></p>
                </li>
                <li>
                    <span class="base steel-color-bkg"></span>
                    <p><em></em><br><kbd>$steel</kbd></p>
                </li>
                <li>
                    <span class="base charcoal-color-bkg"></span>
                    <p><em></em><br><kbd>$charcoal</kbd></p>
                </li>
                <li>
                    <span class="base tuatara-color-bkg"></span>
                    <p><em></em><br><kbd>$tuatara</kbd></p>
                </li>
                <li>
                    <span class="base oil-color-bkg"></span>
                    <p><em></em><br><kbd>$oil</kbd></p>
                </li>
                <li>
                    <span class="base jet-color-bkg"></span>
                    <p><em></em><br><kbd>$jet</kbd></p>
                </li>
                 <li>
                    <span class="base black-color-bkg"></span>
                    <p><em></em><br><kbd>$black</kbd></p>
                </li>
            </ul>
        </div>
    </div>


