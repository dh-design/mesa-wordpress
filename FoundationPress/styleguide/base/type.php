
    <div class="row">
        <div class="columns small-12">
            <div class="section_title">
                <h4>Type</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="columns small-12">
            <h4 class="sg_pattern_heading">Headers</h4>
            <h1>h1. This is a very large header.</h1>
            <h2>h2. This is a large header.</h2>
            <h3>h3. This is a medium header.</h3>
            <h4>h4. This is a moderate header.</h4>
            <h5>h5. This is a small header.</h5>
            <h6>h6. This is a tiny header.</h6>
        </div>
    </div>

    <div class="row">
        <div class="columns small-12">
            <h4 class="sg_pattern_heading">Headers with Links</h4>
            <h1><a href="#">h1. This is a very large header.</a></h1>
            <h2><a href="#">h2. This is a large header.</a></h2>
            <h3><a href="#">h3. This is a medium header.</a></h3>
            <h4><a href="#">h4. This is a moderate header.</a></h4>
            <h5><a href="#">h5. This is a small header.</a></h5>
            <h6><a href="#">h6. This is a tiny header.</a></h6>
        </div>
    </div>

    <div class="row">
        <div class="columns small-12">
            <h4 class="sg_pattern_heading">Subheader</h4>
            <h1 class="subheader">h1. subheader</h1>
            <h2 class="subheader">h2. subheader</h2>
            <h3 class="subheader">h3. subheader</h3>
            <h4 class="subheader">h4. subheader</h4>
            <h5 class="subheader">h5. subheader</h5>
            <h6 class="subheader">h6. subheader</h6>
        </div>
    </div>

    <div class="row">
        <div class="columns small-12">
            <h4 class="sg_pattern_heading">Definition List</h4>
            <p>Definition lists are great for small block of copy that describe the header</p>
            <dl>
                <dt>Lower cost</dt>
                <dd>
                    The new version of this product costs significantly less than the previous one!
                </dd>
                <dt>Easier to use</dt>
                <dd>
                    We&#39;ve changed the product so that it&#39;s much easier to use!
                </dd>
                <dt>Safe for kids</dt>
                <dd>
                    You can leave your kids alone in a room with this product and they won&#39;t get hurt (not a guarantee).
                </dd>
            </dl>
        </div>
    </div>
    <div class="row">
        <div class="columns small-12">            
            <h4 class="sg_pattern_heading">Un-ordered Lists</h4>
            <p>Un-ordered lists are great for making quick outlines bulleted.</p>
            <ul class="disc">
                <li>List item with a much longer description or more content.</li>
                <li>List item</li>
                <li>
                    List item
                    <ul>
                        <li>Nested List Item</li>
                        <li>Nested List Item</li>
                        <li>Nested List Item</li>
                    </ul>
                </li>
                <li>List item</li>
                <li>List item</li>
                <li>List item</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="columns small-12">
            <h4 class="sg_pattern_heading">Ordered Lists</h4>
            <p>Ordered lists are great for lists that need order, duh.</p>
            <ol>
                <li>List Item 1</li>
                <li>List Item 2</li>
                <li>List Item 3</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="columns small-12">
            <h4 class="sg_pattern_heading">Paragraph</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat vulputate porta. Nullam porta, nisl at feugiat aliquam, risus purus fermentum magna, sit amet hendrerit dui tellus a dolor. Suspendisse vehicula, lectus a rhoncus accumsan, lectus dolor facilisis urna, eu pretium ligula metus a lorem. Vivamus ornare eget arcu et blandit. Donec sed elit quis risus convallis consequat. Praesent varius turpis dolor, ac luctus libero interdum at. Morbi varius ullamcorper nulla, a interdum mi. Vivamus vel enim id nibh consectetur convallis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus aliquet elit sed lacinia sodales. Cras at quam diam. In vel enim ut sapien ornare iaculis vitae vel nisi. Aenean ligula nisl, scelerisque at cursus sit amet, efficitur sed lacus. Fusce sodales magna eu elementum hendrerit. Morbi sodales elit vel arcu mattis fermentum. Etiam dapibus quam blandit gravida suscipit.</p>
        </div>
    </div>
    <div class="row">
        <div class="columns small-12">
            <h4 class="sg_pattern_heading">Inline Elements</h4>
            <p>
                <a href="#">This is a text link</a><br>
                The <strong>strong element</strong> and <b>b element</b> example<br>
                The <em>em element</em> and <i>i element</i> example<br>
                The <u>u element</u><br>
                <s>This text has a strikethrough</s><br>
                The <mark>mark element</mark> indicates a highlight<br>
                Superscript<sup>®</sup><br>
                Subscript for things like H<sub>2</sub>O<br>
                <small>This small text is small for for fine print, etc.</small><br>
                <del>This text is deleted</del> and <ins>This text is inserted</ins><br>
                Abbreviation: <abbr title="HyperText Markup Language">HTML</abbr><br>
                <cite>This is a citation</cite><br>
                <code>This is what inline code looks like.</code><br>
                Keybord input: <kbd>Cmd</kbd><br>
                <samp>This is sample output from a computer program</samp>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="columns small-12">
            <h4 class="sg_pattern_heading">Blockquote</h4>
            <blockquote>
                I do not fear computers. I fear the lack of them. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed consectetur.
                <cite>Isaac Asimov</cite>
            </blockquote>
        </div>
    </div>

    <div class="row">
        <div class="columns small-12">      
            <h4 class="sg_pattern_heading">Vcard</h4>
            <ul class="vcard">
                <li class="fn">Gaius Baltar</li>
                <li class="street-address">123 Colonial Ave.</li>
                <li class="locality">Caprica City</li>
                <li>
                    <span class="state">Caprica</span>
                    ,
                    <span class="zip">12345</span>
                </li>
                <li class="email">
                    <a href="#">
                        <span class="__cf_email__" data-cfemail="81e6afe3e0edf5e0f3c1e4f9e0ecf1ede4afe2eeec">[email&nbsp;protected]</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="columns small-12">
            <h4 class="sg_pattern_heading">Horizonal Rule</h4>
            <hr>
        </div>
    </div>
