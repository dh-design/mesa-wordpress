    <div class="row">
        <div class="small-12 columns">
            <div class="section_title">
                <h4>Font</h4>
            </div>
            <p class="font-serif"><strong>Serif Font:</strong> 'Bitter', Georgia, serif; <kbd>$serif</kbd></p>
            <p class="font-serif">
                <kbd>400</kbd> <span class="font-400">The five boxing wizards jump quickly. <em>Brawny gods just flocked up to quiz and vex him.</em></span><br>
                <kbd>700</kbd> <span class="font-700">The five boxing wizards jump quickly. <em>Brawny gods just flocked up to quiz and vex him.</em></span><br>
            </p>
        </div>
    </div>
    <div class="spacer-small"></div>
    <div class="row">
        <div class="small-12 columns">
            <p class="font-sans-serif"><strong>Sans Serif Font:</strong> 'Roboto', Arial, sans-serif; <kbd>$sans-serif</kbd></p>
            <p class="font-sans-serif">
                <kbd>400</kbd> <span class="font-400">The five boxing wizards jump quickly. <em>Brawny gods just flocked up to quiz and vex him.</em></span><br>
                <kbd>700</kbd> <span class="font-700">The five boxing wizards jump quickly. <em>Brawny gods just flocked up to quiz and vex him.</em></span><br>
                <kbd>800</kbd> <span class="font-800">The five boxing wizards jump quickly. <em>Brawny gods just flocked up to quiz and vex him.</em></span><br>
            </p>
        </div>
    </div>
    <div class="spacer-small"></div>
    <div class="row">
        <div class="small-12 columns">
            <p class="font-sans-serif-condensed"><strong>Sans Serif Condensed Font:</strong> 'Roboto Condensed', Arial, sans-serif; <kbd>$sans-serif-condensed</kbd></p>
            <p class="font-sans-serif-condensed">
                <kbd>400</kbd> <span class="font-400">The five boxing wizards jump quickly. <em>Brawny gods just flocked up to quiz and vex him.</em></span><br>
                <kbd>700</kbd> <span class="font-700">The five boxing wizards jump quickly. <em>Brawny gods just flocked up to quiz and vex him.</em></span><br>
            </p>
        </div>
    </div>