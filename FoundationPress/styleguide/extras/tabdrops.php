
    <div class="row">
        <div class="columns large-12">
            <div class="section_title">
                <h4>Tabdrops</h4>
            </div>
        </div>
    </div>

    <div class="row snippet">
        <div class="columns small-11 small-centered">
            <div class="tabdrops" id="specs">
                <ul class="tab-menu">
                    <li class="active">
                        <a href="#tab1" data-toggle="tab">Section 1</a>
                    </li>
                    <li>
                        <a href="#tab2" data-toggle="tab">Section 2</a>
                    </li>
                    <li>
                        <a href="#tab3" data-toggle="tab">Section 3</a>
                    </li>
                    <li>
                        <a href="#tab4" data-toggle="tab">Section 4</a>
                    </li>
                    <li>
                        <a href="#tab5" data-toggle="tab">Section 5</a>
                    </li>
                    <li>
                        <a href="#tab6" data-toggle="tab">Section 6</a>
                    </li>
                    <li>
                        <a href="#tab7" data-toggle="tab">Section 7</a>
                    </li>
                    <li>
                        <a href="#tab8" data-toggle="tab">Section 8</a>
                    </li>
                    <li>
                        <a href="#tab9" data-toggle="tab">Section 9</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <h6>I'm in Section 1.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae efficitur ante, ornare pulvinar elit. Mauris vel placerat diam. Aliquam placerat id dui dignissim feugiat. Phasellus in efficitur risus. Aenean pulvinar semper ante, vehicula sodales mi dapibus nec. Mauris facilisis orci nulla, et molestie mauris aliquam id. Nam accumsan erat at efficitur fermentum. Cras accumsan accumsan nulla, a convallis velit porttitor in. Proin condimentum quis lectus non scelerisque. Etiam sollicitudin nisi nec sodales ultricies.</p>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <h6>I'm in Section 2.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae efficitur ante, ornare pulvinar elit. Mauris vel placerat diam. Aliquam placerat id dui dignissim feugiat. Phasellus in efficitur risus. Aenean pulvinar semper ante, vehicula sodales mi dapibus nec. Mauris facilisis orci nulla, et molestie mauris aliquam id. Nam accumsan erat at efficitur fermentum. Cras accumsan accumsan nulla, a convallis velit porttitor in. Proin condimentum quis lectus non scelerisque. Etiam sollicitudin nisi nec sodales ultricies.</p>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <h6>I'm in Section 3.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae efficitur ante, ornare pulvinar elit. Mauris vel placerat diam. Aliquam placerat id dui dignissim feugiat. Phasellus in efficitur risus. Aenean pulvinar semper ante, vehicula sodales mi dapibus nec. Mauris facilisis orci nulla, et molestie mauris aliquam id. Nam accumsan erat at efficitur fermentum. Cras accumsan accumsan nulla, a convallis velit porttitor in. Proin condimentum quis lectus non scelerisque. Etiam sollicitudin nisi nec sodales ultricies.</p>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <h6>I'm in Section 4.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae efficitur ante, ornare pulvinar elit. Mauris vel placerat diam. Aliquam placerat id dui dignissim feugiat. Phasellus in efficitur risus. Aenean pulvinar semper ante, vehicula sodales mi dapibus nec. Mauris facilisis orci nulla, et molestie mauris aliquam id. Nam accumsan erat at efficitur fermentum. Cras accumsan accumsan nulla, a convallis velit porttitor in. Proin condimentum quis lectus non scelerisque. Etiam sollicitudin nisi nec sodales ultricies.</p>
                    </div>
                    <div class="tab-pane" id="tab5">
                        <h6>I'm in Section 5.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae efficitur ante, ornare pulvinar elit. Mauris vel placerat diam. Aliquam placerat id dui dignissim feugiat. Phasellus in efficitur risus. Aenean pulvinar semper ante, vehicula sodales mi dapibus nec. Mauris facilisis orci nulla, et molestie mauris aliquam id. Nam accumsan erat at efficitur fermentum. Cras accumsan accumsan nulla, a convallis velit porttitor in. Proin condimentum quis lectus non scelerisque. Etiam sollicitudin nisi nec sodales ultricies.</p>
                    </div>
                    <div class="tab-pane" id="tab6">
                        <h6>I'm in Section 6.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae efficitur ante, ornare pulvinar elit. Mauris vel placerat diam. Aliquam placerat id dui dignissim feugiat. Phasellus in efficitur risus. Aenean pulvinar semper ante, vehicula sodales mi dapibus nec. Mauris facilisis orci nulla, et molestie mauris aliquam id. Nam accumsan erat at efficitur fermentum. Cras accumsan accumsan nulla, a convallis velit porttitor in. Proin condimentum quis lectus non scelerisque. Etiam sollicitudin nisi nec sodales ultricies.</p>
                    </div>
                    <div class="tab-pane" id="tab7">
                        <h6>I'm in Section 7.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae efficitur ante, ornare pulvinar elit. Mauris vel placerat diam. Aliquam placerat id dui dignissim feugiat. Phasellus in efficitur risus. Aenean pulvinar semper ante, vehicula sodales mi dapibus nec. Mauris facilisis orci nulla, et molestie mauris aliquam id. Nam accumsan erat at efficitur fermentum. Cras accumsan accumsan nulla, a convallis velit porttitor in. Proin condimentum quis lectus non scelerisque. Etiam sollicitudin nisi nec sodales ultricies.</p>
                    </div>
                    <div class="tab-pane" id="tab8">
                        <h6>I'm in Section 8.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae efficitur ante, ornare pulvinar elit. Mauris vel placerat diam. Aliquam placerat id dui dignissim feugiat. Phasellus in efficitur risus. Aenean pulvinar semper ante, vehicula sodales mi dapibus nec. Mauris facilisis orci nulla, et molestie mauris aliquam id. Nam accumsan erat at efficitur fermentum. Cras accumsan accumsan nulla, a convallis velit porttitor in. Proin condimentum quis lectus non scelerisque. Etiam sollicitudin nisi nec sodales ultricies.</p>
                    </div>
                    <div class="tab-pane" id="tab9">
                        <h6>I'm in Section 9.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae efficitur ante, ornare pulvinar elit. Mauris vel placerat diam. Aliquam placerat id dui dignissim feugiat. Phasellus in efficitur risus. Aenean pulvinar semper ante, vehicula sodales mi dapibus nec. Mauris facilisis orci nulla, et molestie mauris aliquam id. Nam accumsan erat at efficitur fermentum. Cras accumsan accumsan nulla, a convallis velit porttitor in. Proin condimentum quis lectus non scelerisque. Etiam sollicitudin nisi nec sodales ultricies.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

   