
<div class="row">
    <div class="columns large-12 ">
        <div class="section_title">
            <h4>Slick Carousel</h4>
        </div>
    </div>
</div>
<div class="row snippet">
	<div class="columns medium-8 small-centered">
		<div class="carousel single no-dots">
		  	<div>
		  		<div class="container">
		  			<img src="http://placehold.it/700x425" class="center">
		  		</div>
		  	</div>
		  	<div>
		  		<div class="container">
		  			<img src="http://placehold.it/700x425" class="center">
		  		</div>
		  	</div>
		  	<div>
		  		<div class="container">
		  			<img src="http://placehold.it/700x425" class="center">
		  		</div>
		  	</div>
		</div>
	</div>
</div>
