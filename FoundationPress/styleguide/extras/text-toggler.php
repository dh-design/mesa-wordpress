<section id="text-toggler">
	<div class="row">
		<div class="small-12 columns">
			<div class="section_title">
                <h4>Text Toggler</h4>
            </div>
		</div>
	</div>
	<div class="row snippet">
		<div class="small-12 columns">
			<div class="toggle_wrap general-toggler">
				<ul>
					<li>
						<h6 class="toggler"><a href="#">I am the beginning of the end, and the end of time and space. I am essential to creation, and I surround every place. What am I?</a></h6>
						<div class="togglee">
							 <p>Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis euismod semper.</p>
						</div>
					</li>
					<li>
						<h6 class="toggler"><a href="#">What always runs but never walks, often murmurs, never talks, has a bed but never sleeps, has a mouth but never eats?</a></h6>
						<div class="togglee">
							<p>Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis euismod semper.</p>
						</div>
					</li>
					<li>
						<h6 class="toggler"><a href="#">Our Philosophy</a></h6>
						<div class="togglee">
							<p>Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis euismod semper.</p>
							<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec id elit non mi porta gravida at eget metus.</p>
						</div>
					</li>
					<li>
						<h6 class="toggler"><a href="#">Our Director</a></h6>
						<div class="togglee">
							<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus. Aenean lacinia bibendum nulla sed consectetur. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit.<p>
						</div>
					</li>
			    </ul>
			</div>
		</div>
	</div>
</section>
