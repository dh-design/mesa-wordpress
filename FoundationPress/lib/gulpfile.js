const args = require('minimist')(process.argv.slice(2))
const chalk = require('chalk')
const gulp = require('gulp')
const sourcemaps = require('gulp-sourcemaps')
const sass = require('gulp-sass')
const uglify = require('gulp-uglify')
const concat = require('gulp-concat')
const autoprefixer = require('gulp-autoprefixer')
const gulpif = require('gulp-if')
const tap = require('gulp-tap')
const end = require('stream-end')
const prepend = require('prepend-file')
const slashes = require('slashes')
const fs = require('fs')
const cleanCSS = require('gulp-clean-css')
const babel = require('gulp-babel')
const sassWatchSrc = ['./styles/**/*.scss', './styles/*.scss']
const sassSrc = './styles/*.scss'
const sassDest = './styles/css'
const jsSrc = [
  // JQUERY v2.1.1  | UI - v1.11.4
  './js/jquery.min.js',
  './js/jquery-ui.min.js',

  // FOUNDATION CORE
  './js/foundation/foundation.core.js',
  './js/foundation/foundation.util.mediaQuery.js',
  './js/foundation/foundation.util.box.js',
  './js/foundation/foundation.util.keyboard.js',
  './js/foundation/foundation.util.motion.js',
  './js/foundation/foundation.util.nest.js',
  './js/foundation/foundation.util.timerAndImageLoader.js',
  './js/foundation/foundation.util.touch.js',
  './js/foundation/foundation.util.triggers.js',
  './js/foundation/foundation.abide.js',
  './js/foundation/foundation.accordion.js',
  './js/foundation/foundation.accordionMenu.js',
  './js/foundation/foundation.drilldown.js',
  './js/foundation/foundation.dropdown.js',
  './js/foundation/foundation.dropdownMenu.js',
  './js/foundation/foundation.equalizer.js',
  './js/foundation/foundation.interchange.js',
  './js/foundation/foundation.magellan.js',
  './js/foundation/foundation.offcanvas.js',
  './js/foundation/foundation.responsiveMenu.js',
  './js/foundation/foundation.responsiveToggle.js',
  './js/foundation/foundation.reveal.js',
  './js/foundation/foundation.slider.js',
  './js/foundation/foundation.sticky.js',
  './js/foundation/foundation.tabs.js',
  './js/foundation/foundation.toggler.js',
  './js/foundation/foundation.tooltip.js',

  // TABDROPS
  './js/plugins/tabdrops/bootstrap-tab.js',
  './js/plugins/tabdrops/tabdrops.js',

  // // EXTRAS
  './js/plugins/slick-carousel/slick.min.js',
  // './js/plugins/select2/select2.min.js',
  // './js/plugins/mixitup/jquery.mixitup.js',
  './js/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js',
  // './js/plugins/greensock/TweenMax.min.js',
  // './js/plugins/greensock/addons/ScrollToPlugin.min.js',
  // './js/plugins/scrollmagic/ScrollMagic.js',
  // './js/plugins/scrollmagic/addons/animation.gsap.js',
  // './js/plugins/scrollmagic/addons/debug.addIndicators.js',

  // // AUTO
  './js/plugins/fastclick.js',
  './js/plugins/jquery.autocomplete.js',
  './js/plugins/jquery.cookie.js',
  './js/plugins/placeholder.js',
  // './js/plugins/richmarker.js',
  './js/global.js'

]
const jsWatchSrc = jsSrc
const jsDest = './js/'
const errFile = './js/.errors.js'
let sassWasSuccessLastTime = true

jsSrc.push(errFile)

fs.writeFileSync(errFile, '')

gulp.task('sass', function (done) {
  let success = true

  if (args.watch) {
    gulp.watch(sassWatchSrc, ['sass'])
  }

  gulp.src(sassSrc)
    .pipe(sourcemaps.init())
    .pipe(sass.sync().on('error', function (err) {
      console.error(`sass: ${chalk.red(err.message)}`)
      prepend.sync(errFile, `console.error("${slashes.add(err.message)}")\n`)
      success = false

      done()
    }))
    .pipe(autoprefixer())
    .pipe(gulpif(!args.dev, cleanCSS({debug: true}, function (details) {
      console.log(`clean-css: ${details.name} ${details.stats.originalSize - details.stats.minifiedSize} bytes saved`)
    })))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(sassDest))
    .pipe(tap(function () {
      if (success) {
        if (!sassWasSuccessLastTime) {
          fs.writeFileSync(errFile, '')
        }
      }
      sassWasSuccessLastTime = success
    }))
    .pipe(end(done))
})

gulp.task('js', function () {
  if (args.watch) {
    gulp.watch(jsWatchSrc, ['js'])
  }

  return gulp.src(jsSrc)
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: [['env', {
        'modules': false
      }]
      ]
    }))
    .pipe(gulpif(!args.dev, uglify()))
    .pipe(concat('combined.min.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(jsDest))
})

gulp.task('default', ['sass', 'js'])
