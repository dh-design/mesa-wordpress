

$(document).ready(function(){

/* ==========================================================================
0.) TEXT TOGGLER
========================================================================== */
    /* Accordian collapser */
    $('.toggle_wrap .togglee').each(function() {
      if (!$(this).hasClass('default-open')) {
        $(this).hide();
      }
    });

    $(".toggler").click(function() {
      if ($(this).parents('.toggle_wrap').length >= 1) {
        var accordian = $(this).parents('.toggle_wrap');
        if ($(this).hasClass('active')) {
          $(accordian).find('.toggler').removeClass('active');
          $(accordian).find(".togglee").slideUp();
        } else {
          $(accordian).find('.toggler').removeClass('active');
          $(accordian).find(".togglee").slideUp();
          $(this).addClass('active');
          $(this).next(".togglee").slideToggle();
        }
      } else {
        if ($(this).hasClass('active')) {
          $(this).removeClass("active");
        } else {
          $(this).addClass("active");
        }
      }
      return false;
    });

    $(".toggler").click(function() {
      if (!$(this).parents('.toggle_wrap').length >= 1) {
        $(this).next(".togglee").slideToggle();
      }
    });



/* ==========================================================================
0.) LINES BUTTON -- TABLET/MOBILE NAVIGATION HAMBURGER
========================================================================== */

    $("a.lines-button").on('click', function(){
        lineButton = $(this);
        var offCanvasWrapper = lineButton.parents('.off-canvas-wrapper').find('.off-canvas');

        if(!offCanvasWrapper.hasClass('is-open')){
            lineButton.addClass('close');
        }else{
            lineButton.removeClass('close');
        }

        $('.js-off-canvas-overlay').on('click', function(){

            if(!$(this).hasClass('is-visible')){
                lineButton.removeClass('close');
            }else{
                lineButton.addClass('close');
            }

        });

    });



/* ==========================================================================
0.) EQUAL HEIGHT
========================================================================== */


equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('*[data-equalize]');
});


$(window).resize(function(){
  equalheight('*[data-equalize]');
});



/* ==========================================================================
0.8) SIDEBAR CHECK WIDTH
========================================================================== */
    // This changes the class on a sidenav / sidebar so that we are not repeating
    // content on mobile -- if using offcanvans...

    function sidebarCheckWidth() {
        var windowsize = window.innerWidth;

        if (windowsize <= 1024) {
           $('#sidebar').removeClass('columns large-3');
           $('#sidebar').addClass('left-off-canvas-menu');
        }
        if (windowsize >= 1025) {
           $('#sidebar').removeClass('left-off-canvas-menu');
           $('#sidebar').addClass('columns large-3');
        }
    }
    // Execute on load
    sidebarCheckWidth();
    // Bind event listener
    $(window).resize(sidebarCheckWidth);


/* ==========================================================================
# /////////////////////////// SLIDER/GALLERY ////////////////////////////////
========================================================================== */


/* ==========================================================================
2.1)  SLICK SLIDER
========================================================================== */


    // //CAROUSEL SINGLE (Single && Arrow Buttons)
    // if($(".carousel.single-w-btns").length){
    //     $('.carousel.single-w-btns').slick({
    //         infinite: true,
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         adaptiveHeight: true,
    //         arrows: true,
    //         appendArrows: '.append-arrows',
    //         dots: true,
    //     });
    //     $('.carousel.single-w-btns').fadeIn();
    // }

    // CAROUSEL SINGLE
    if($(".carousel.single").length){
        $('.carousel.single').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true,
            arrows: true,
            dots: true
        });
        $('.carousel.single').fadeIn();
    }

    // // IMAGE MAIN SLIDER W/DESCRIPTION
    // // Used on product inspiration.
    // if($(".carousel.image-main").length){
    //     $('.carousel.image-main').slick({
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         arrows: true,
    //         fade: true,
    //         asNavFor: '.description-sub'
    //     });

    //     $('.carousel.description-sub').slick({
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         asNavFor: '.img-main',
    //         dots: false,
    //         arrows:false,
    //         fade: true,
    //         centerMode: false,
    //         focusOnSelect: true
    //     });
    // }

    // // PRODUCT DESCRIPTION W/IMAGE
    // // Used on the Homepage alt
    // if($(".carousel.product-description").length){
    //     $('.carousel.product-description').slick({
    //         infinite: true,
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         adaptiveHeight: true,
    //         arrows: true,
    //         fade: true,
    //         appendArrows: '.append-arrows',
    //         asNavFor: '.product-image'
    //     });
    //     $('.carousel.product-image').slick({
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         asNavFor: '.product-description',
    //         dots: false,
    //         arrows:false,
    //         fade: true,
    //         centerMode: false,
    //         focusOnSelect: true
    //     });
    // }

    // // FULLWIDTH
    // if($(".carousel.full-width").length){
    //     $('.carousel.full-width').slick({
    //         centerMode: true,
    //         centerPadding: '1200px',
    //         slidesToShow: 3,
    //         slidesToScroll: 1,
    //         variableWidth: true,
    //         autoplay: true,
    //         autoplaySpeed: 7000,
    //             responsive: [
    //             {
    //                 breakpoint: 1500,
    //                 settings: {
    //                     centerPadding: '900px',
    //                 }
    //             },
    //             {
    //                 breakpoint: 1270,
    //                 settings: {
    //                     centerPadding: '400px',
    //                 }
    //             },
    //             {
    //                 breakpoint: 641,
    //                 settings: {
    //                     centerMode: true,
    //                     centerPadding: '60px',
    //                     slidesToShow: 1,
    //                     variableWidth: false,
    //                     adaptiveHeight: true
    //                 }
    //             }
    //         ]
    //     });
    // }



/* ==========================================================================
2.3)  TABDROP
========================================================================== */

// Tabdrops
$('#specs .tab-menu').tabdrop();





});

$(document).foundation();