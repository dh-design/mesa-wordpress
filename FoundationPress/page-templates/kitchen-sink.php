<?php
/*
Template Name: Kitchen Sink
*/
get_header(); ?>


	<?php /* Start loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>

	<?php wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/lib/styles/css/styleguide.css', array(), '2.9.2', 'all' ); ?>

	<div class="main-wrap" role="main">
		<div class="row">
			<div class="columns small-12">
				<header class="kitchen-sink-header">
					<h1 class="entry-title"><?php the_title(); ?></h1><hr>
					<p class="lead">This page includes every single Foundation element so that we can make sure things work together smoothly.</p><hr>
				</header>

				<!-- Main wrapper for the components in the kitchen-sink -->
				<div id="components" class="kitchen-sink-components">
					<div class="row">
						<div class="columns large-9 large-push-3">
							<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

								<!-- Base ============================================= -->
								<section>
						            <div class="row">
						                <div class="columns small-12">
						                    <h2 class="no-margin-top"><span>Base</span></h2>
						                </div>
						            </div>
						        </section>

						        <!-- Abide -->
						        <section id="colors"  data-magellan-target="colors">
						            <?php get_template_part('styleguide/base/colors'); ?>
						        </section>

						        <!-- Fonts -->
						        <section id="fonts" data-magellan-target="fonts">
						            <?php get_template_part('styleguide/base/fonts'); ?>
						        </section>
						        
						        <!-- Type -->
						        <section id="type" data-magellan-target="type">
						            <?php get_template_part('styleguide/base/type'); ?>
						        </section>
						        
						        <!-- Icons -->
						        <section id="icons" data-magellan-target="icons">
						            <?php get_template_part('styleguide/base/icons'); ?>
						        </section>


						        <!-- Foundation ============================================= -->
								<section>
						            <div class="row">
						                <div class="columns small-12">
						                    <h2 class="margin-top-large"><span>Foundation</span></h2>
						                </div>
						            </div>
						        </section>

								<!-- Abide -->
								<section id="abide" data-magellan-target="abide">
									<?php get_template_part('styleguide/foundation/abide'); ?>
								</section>

								<!-- Accordion -->
								<section id="accordion" data-magellan-target="accordion">
									<?php get_template_part('styleguide/foundation/accordion'); ?>
								</section>

								<!-- Accordion Menu -->
								<section id="accordion-menu" data-magellan-target="accordion-menu">
									<?php get_template_part('styleguide/foundation/accordion-menu'); ?>
								</section>

								<!-- Breadcrumbs -->
								<section id="breadcrumbs" data-magellan-target="breadcrumbs">
									<?php get_template_part('styleguide/foundation/breadcrumbs'); ?>
								</section>

								<!-- Buttons -->
								<section id="button" data-magellan-target="button">
									<?php get_template_part('styleguide/foundation/button'); ?>
								</section>

								<!-- Callout -->
								<section id="callout" data-magellan-target="callout">
									<?php get_template_part('styleguide/foundation/callout'); ?>
								</section>

								<!-- Close Button -->
								<section id="close-button" data-magellan-target="close-button">
									<?php get_template_part('styleguide/foundation/close-button'); ?>
								</section>

								<!-- Drilldown Menu -->
								<section id="drilldown-menu" data-magellan-target="drilldown-menu">
									<?php get_template_part('styleguide/foundation/drilldown-menu'); ?>
								</section>

								<!-- Dropdown Menu -->
								<section id="dropdown-menu" data-magellan-target="dropdown-menu">
									<?php get_template_part('styleguide/foundation/dropdown-menu'); ?>
								</section>

								<!-- Equalizer -->
								<section id="equalizer" data-magellan-target="equalizer">
									<?php get_template_part('styleguide/foundation/equalizer'); ?>
								</section>

								<!-- Flex Video -->
								<section id="flex-video" data-magellan-target="flex-video">
									<?php get_template_part('styleguide/foundation/flex-video'); ?>
								</section>

								<!-- Float Classes -->
								<section id="float-classes" data-magellan-target="float-classes">
									<?php get_template_part('styleguide/foundation/float-classes'); ?>
								</section>

								<!-- Forms -->
								<section id="forms" data-magellan-target="forms">
									<?php get_template_part('styleguide/foundation/forms'); ?>
								</section>

								<!-- Grid -->
								<section id="grid" data-magellan-target="grid">
									<?php get_template_part('styleguide/foundation/grid'); ?>
								</section>

								<!-- Interchange -->
								<section id="interchange" data-magellan-target="interchange">
									<?php get_template_part('styleguide/foundation/interchange'); ?>
								</section>

								<!-- Magellan -->
								<section id="magellan" data-magellan-target="magellan">
									<?php get_template_part('styleguide/foundation/magellan'); ?>
								</section>

								<!-- Menu -->
								<section id="menu" data-magellan-target="menu">
									<?php get_template_part('styleguide/foundation/menu'); ?>
								</section>

								<!-- Pagination -->
								<section id="pagination" data-magellan-target="pagination">
									<?php get_template_part('styleguide/foundation/pagination'); ?>
								</section>

								<!-- Reveal -->
								<section id="reveal" data-magellan-target="reveal">
									<?php get_template_part('styleguide/foundation/reveal'); ?>
								</section>

								<!-- Sticky -->
								<section id="sticky" data-magellan-target="sticky">
									<?php get_template_part('styleguide/foundation/sticky'); ?>
								</section>

								<!-- Table -->
								<section id="table" data-magellan-target="table">
									<?php get_template_part('styleguide/foundation/table'); ?>
								</section>

								<!-- Tabs -->
								<section id="tabs" data-magellan-target="tabs">
									<?php get_template_part('styleguide/foundation/tabs'); ?>
								</section>

								<!-- Title Bar -->
								<section id="title-bar" data-magellan-target="title-bar">
									<?php get_template_part('styleguide/foundation/title-bar'); ?>
								</section>

								<!-- Top Bar -->
								<section id="top-bar" data-magellan-target="top-bar">
									<?php get_template_part('styleguide/foundation/top-bar'); ?>
								</section>

								<!-- Toggler -->
								<section id="toggler" data-magellan-target="toggler">
									<?php get_template_part('styleguide/foundation/toggler'); ?>
								</section>

								<!-- Tooltips -->
								<section id="tooltips" data-magellan-target="tooltips">
									<?php get_template_part('styleguide/foundation/tooltips'); ?>
								</section>


								<!-- Visibility Classes -->
								<section id="visibility-classes" data-magellan-target="visibility-classes">
									<?php get_template_part('styleguide/foundation/visibility-classes'); ?>
								</section>



								<!-- Extras ============================================= -->
								<section>
						            <div class="row">
						                <div class="columns small-12">
						                    <h2 class="margin-top-large"><span>Extras</span></h2>
						                </div>
						            </div>
						        </section>

						        <!-- Slick Carousel -->
								<section id="slick-carousel" data-magellan-target="slick-carousel">
									<?php get_template_part('styleguide/extras/slick-carousel'); ?>
								</section>
 
								<!-- Text Toggler -->
								<section id="text-toggler" data-magellan-target="text-toggler">
									<?php get_template_part('styleguide/extras/text-toggler'); ?>
								</section>

								<!-- Tabdrops -->
								<section id="tabdrops" data-magellan-target="tabdrops">
									<?php get_template_part('styleguide/extras/tabdrops'); ?>
								</section>


							</article>
						</div>
						<div class="columns large-3 large-pull-9">
						<!-- On this page - sidebar nav container -->
						<nav id="kitchen-sink-nav" class="kitchen-sink-nav">
							<div class="docs-toc" data-anchor="components">
								<ul class="vertical menu docs-sub-menu" data-magellan data-accordion-menu>
									<li class="docs-menu-title">On this page:</li>
									<li>
										<a href="#">Base</a>
										<ul class="menu vertical nested">
											<li><a href="#colors">Colors</a></li>
											<li><a href="#fonts">Fonts</a></li>
											<li><a href="#type">Type</a></li>
											<li><a href="#icons">Icons</a></li>
										</ul>
									</li>
									<li>
										<a href="#">Foundation</a>
										<ul class="menu vertical nested">
											<li><a href="#abide">Abide</a></li>
											<li><a href="#accordion">Accordion</a></li>
											<li><a href="#accordion-menu">Accordion Menu</a></li>
											<li><a href="#breadcrumbs">Breadcrumbs</a></li>
											<li><a href="#button">Button</a></li>
											<li><a href="#callout">Callout</a></li>
											<li><a href="#close-button">Close button</a></li>
											<li><a href="#drilldown-menu">Drilldown Menu</a></li>
											<li><a href="#dropdown-menu">Dropdown Menu</a></li>
											<li><a href="#dropdown-pane">Dropdown Pane</a></li>
											<li><a href="#equalizer">Equalizer</a></li>
											<li><a href="#flex-video">Flex Video</a></li>
											<li><a href="#float-classes">Float Classes</a></li>
											<li><a href="#forms">Forms</a></li>
											<li><a href="#grid">Grid</a></li>
											<li><a href="#menu">Menu</a></li>
											<li><a href="#pagination">Pagination</a></li>
											<li><a href="#reveal">Reveal</a></li>
											<li><a href="#sticky">Sticky</a></li>
											<li><a href="#table">Table</a></li>
											<li><a href="#tabs">Tabs</a></li>
											<li><a href="#title-bar">Title Bar</a></li>
											<li><a href="#top-bar">Top Bar</a></li>
											<li><a href="#toggler">Toggler</a></li>
											<li><a href="#tooltip">Tooltip</a></li>
											<li><a href="#visibility-classes">Visibility Classes</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>	
	</div><!-- Close main-wrap -->

	<div class="entry-content">
		<?php the_content(); ?>
	</div>

	<?php endwhile; ?>
<?php get_footer(); ?>

<script>
     // Color Section
     // This grabs the color and adds the hex# and appends it
     // to the individual card.
    $('.accent, .base').each(function(){
        $(this).next().children('em').html(rgbToHex($(this).css("backgroundColor")));
    });

    function rgbToHex(total) {
            var total = total.split(',');
            var r = total[0].substring(4);
            var g = total[1].substring(1);
            var b = total[2].substring(1,total[2].length-1);
        return ("#"+checkNumber((r*1).toString(16))+checkNumber((g*1).toString(16))+checkNumber((b*1).toString(16))).toUpperCase();
    }
    function checkNumber(i){
        i = i.toString();
        if (i.length == 1) return '0'+i;
        else return i;
    }
</script>


